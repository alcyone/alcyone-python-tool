ARG PYTHON_VERSION=3.12.3

FROM bitnami/python:$PYTHON_VERSION

ARG USER_NAME=alcyone
ARG USER_ID=1001
ARG USER_HOME=/app
ARG PYTHON_VERSION # Docker weirdness to retain ARG across FROM

LABEL org.opencontainers.image.authors="David James Sherman <david.sherman@inria.fr>" \
    org.opencontainers.image.title="Alcyone Python tool s2i image" \
    org.opencontainers.image.description="Python ${PYTHON_VERSION} image for s2i builds with pip and hatch" \
    io.k8s.display-name="Alcyone Python ${PYTHON_VERSION}" \
    io.openshift.expose-services="8080:http" \
    io.openshift.tags="alcyone,builder,python" \
    io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

# Basic Python packaging tools

RUN pip install \
        hatch \
        tox \
        twine \
    && pip cache purge

# Expose the service port

EXPOSE 8080

# Openshift best practices for nonroot containers

RUN useradd --uid ${USER_ID} --groups 0 --create-home ${USER_NAME} --home-dir ${USER_HOME}

RUN chown -R ${USER_ID}:0 ${USER_HOME} &&\
    chmod -R g=u ${USER_HOME} &&\
    find ${USER_HOME} -type d -print0 | xargs -0 chmod g+ws

WORKDIR $USER_HOME
ENV HOME=$USER_HOME
USER $USER_ID

# Explain how to use this image

CMD ["/usr/libexec/s2i/usage"]
